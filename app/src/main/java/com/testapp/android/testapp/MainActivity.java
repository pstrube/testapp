package com.testapp.android.testapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;


public class MainActivity extends ActionBarActivity {

    private final String KEY_COLOR = "color";

    private Button mBtnClear;
    private EditText mEditText;
    private RelativeLayout mMain;
    private int mColorState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set references
        mBtnClear = (Button)findViewById(R.id.button_clear);
        mEditText = (EditText)findViewById(R.id.editText);
        mMain = (RelativeLayout)findViewById(R.id.main);

        mBtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mEditText.setText("");
            }
        });

        // persistence for bg color across orientation changes
        if(savedInstanceState != null){
           mColorState = savedInstanceState.getInt(KEY_COLOR);
           mMain.setBackgroundColor(mColorState);
        } else {
            mMain.setBackgroundColor(getResources().getColor(R.color.blue));
        }// end if
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // blue/red background color options
        if (id == R.id.action_blue) {
            setColor(R.color.blue);

            return true;
        }
        else if (id == R.id.action_red){
            setColor(R.color.red);

            return true;
        }// end if

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        // save color state
        savedInstanceState.putInt(KEY_COLOR, mColorState);
    }

    // Wrote this into a method because the code for both colors doesn't change except for the color
    // IDs. Adroid Studio complains about using an unresolved resource ID, but it doesn't affect
    // things at runtime.
    private void setColor(int background)
    {
        mMain.setBackgroundColor(getResources().getColor(background));
        mColorState = mMain.getResources().getColor(background);
    }

}
